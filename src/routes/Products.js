import React from 'react';
import { connect } from 'dva';
import ProductList from '../components/ProductList';
import ReactDOM from 'react-dom';
import { Calendar } from 'antd';

const Products = ({ dispatch, products }) => {
  function handleDelete(id) {
    dispatch({
      type: 'products/delete',
      payload: id,
    });
  }
  function onPanelChange(value, mode) {
    console.log(value, mode);
  }
  return (
    <div>
      <h2>List of Products</h2>
      <ProductList onDelete={handleDelete} products={products} />
      <div style={{ width: 290, border: '1px solid #d9d9d9', borderRadius: 4 }}>
      <Calendar fullscreen={false} onPanelChange={onPanelChange} />
      </div>
    </div>
  );
};

  
 
// export default Products;
export default connect(({ products }) => ({
  products,
}))(Products);